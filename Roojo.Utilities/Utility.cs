﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using ClosedXML.Excel;
using System.Net.Configuration;
using System.Text.RegularExpressions;

namespace Roojo.Utilities
{
    public class Utility
    {
        /// <summary>
        /// Se debe parametrizar en el app.config de service, en la sección de AppSettings por las Keys de SmtpUserName y SmtpPassword
        /// <add key="SmtpUserName" value="Correo@prueba.com" />
        /// <add key = "SmtpPassword" value="Clavecorreo" />
        /// <add key = "SmtpHost" value="smtp.gmail.com" />
        /// <add key = "SmtpPort" value="587" />
        /// </summary>
        /// <param name="objMail">Objeto de tipo System.Net.Mail.MailMessage</param>
        /// <returns></returns>
        public async Task<Boolean> SendEmail(MailMessage objMail, int smtpTimeOut = 100000)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SmtpHost"].ToString();
                smtp.Port = Convert.ToInt32( ConfigurationManager.AppSettings["SmtpPort"]);
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpUserName"].ToString(), ConfigurationManager.AppSettings["SmtpPassword"].ToString());
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Timeout = smtpTimeOut;
                smtp.Send(objMail);
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Se debe parametrizar en el app.config de service, en la sección de AppSettings por las Keys de SmtpUserName y SmtpPassword
        /// <add key="SmtpUserName" value="Correo@prueba.com" />
        /// <add key = "SmtpPassword" value="Clavecorreo" />
        /// <add key = "SmtpHost" value="smtp.gmail.com" />
        /// <add key = "SmtpPort" value="587" />
        /// </summary>
        /// <param name="From"></param>
        /// <param name="FromName"></param>
        /// <param name="Asunto"></param>
        /// <param name="Body"></param>
        /// <param name="To"></param>
        /// <returns></returns>
        public async Task<Boolean> SendEmail(string From, string FromName, string Asunto, string Body, string To, int smtpTimeOut = 100000)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(From, FromName, Encoding.UTF8);
                mail.Subject = Asunto;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                mail.To.Add(To);

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SmtpHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
                smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpUserName"].ToString(), ConfigurationManager.AppSettings["SmtpPassword"].ToString());
                smtp.EnableSsl = true;
                smtp.Timeout = smtpTimeOut;

                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// </summary>
        /// <param name="objMail">Objeto de tipo System.Net.Mail.MailMessage</param>
        /// <returns></returns>
        public async Task<Boolean> SendMailWithoutCredentials(MailMessage objMail, int smtpTimeOut = 100000)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();

                SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                objMail.From = new MailAddress(section.From, section.From, Encoding.UTF8);

                smtp.Host = section.Network.Host;
                smtp.EnableSsl = false;
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = section.Network.DefaultCredentials;
                smtp.Timeout = smtpTimeOut;
                smtp.Send(objMail);
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="From"></param>
        /// <param name="FromName"></param>
        /// <param name="Asunto"></param>
        /// <param name="Body"></param>
        /// <param name="To"></param>
        /// <returns></returns>
        public async Task<Boolean> SendMailWithoutCredentials(string From, string FromName, string Asunto, string Body, string To, int smtpTimeOut = 100000)
        {
            try
            {
                
                SmtpClient smtp = new SmtpClient();

                SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(section.From, section.From, Encoding.UTF8);
                mail.Subject = Asunto;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                mail.To.Add(To);

                smtp.Host = section.Network.Host;
                smtp.EnableSsl = false;
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = section.Network.DefaultCredentials;
                smtp.Timeout = smtpTimeOut;
                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task EjecutarProcedimientoADO(String NameSP, List<SqlParameter> parametros, int Timeout = 1000)
        {
            await Task.Run(() =>
            {
                var conexion = ConfigurationManager.ConnectionStrings["UtilitiesConnectionString"].ToString();
                using (SqlConnection conn = new SqlConnection(conexion))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand(NameSP, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = Timeout;
                    command.Parameters.AddRange(parametros.ToArray());
                    command.ExecuteNonQuery();
                }
            });
        }

        /// <summary>
        /// Este metodo ejecuta el procedimiento indicado en la variable NameSP ademas recibe una lista de SqlParameter,
        /// donde deben ir los parametros del procedimineto de la siguiente manera.
        /// List<SqlParameter> parameters = new List<SqlParameter>(){
        ///     new SqlParameter() { ParameterName = "@NombreParametro", SqlDbType = SqlDbType.int, Value = El valor que se le va a enviar al parametro}
        ///};
        /// </summary>
        /// <param name="NameSP"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public async Task<DataTable> ExecProcedimientoDataTable(String NameSP, List<SqlParameter> parametros, int Timeout = 1000)
        {
            return await Task.Run(() =>
            {
                DataTable data = new DataTable();
                var conexion = ConfigurationManager.ConnectionStrings["UtilitiesConnectionString"].ToString();
                using (SqlConnection conn = new SqlConnection(conexion))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand(NameSP, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = Timeout;
                    command.Parameters.AddRange(parametros.ToArray());
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(data);
                }

                return data;
            });
        }
        
        public async Task<DataSet> ExecProcedimientoDataSet(String NameSP, List<SqlParameter> parametros, int Timeout = 1000)
        {
            return await Task.Run(() =>
            {
                DataSet data = new DataSet();
                var conexion = ConfigurationManager.ConnectionStrings["UtilitiesConnectionString"].ToString();
                using (SqlConnection conn = new SqlConnection(conexion))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand(NameSP, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = Timeout;
                    command.Parameters.AddRange(parametros.ToArray());
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(data);
                }

                return data;
            });
        }

        /// <summary>
        /// Convierte una hoja de un archivo de excel a un dataset
        /// </summary>
        /// <param name="RutadeExcel"></param>
        /// <returns></returns>
        public DataSet DataSetExcel(string RutadeExcel)
        {
            DataSet Result = new DataSet();
            var LibroExcel = new XLWorkbook(RutadeExcel);

            foreach (IXLWorksheet HojaExcel in LibroExcel.Worksheets)
            {
                DataTable DTDatos = new DataTable();
                DTDatos.TableName = HojaExcel.Name;
                var range = HojaExcel.Range(HojaExcel.FirstCellUsed(), HojaExcel.LastCellUsed());

                int col = range.ColumnCount();
                int row = range.RowCount();

                // add columns hedars
                DTDatos.Clear();

                for (int i = 1; i <= col; i++)
                {
                    IXLCell column = HojaExcel.Cell(1, i);
                    DTDatos.Columns.Add(column.Value.ToString());
                }

                // add rows data   
                int firstHeadRow = 0;
                foreach (var item in range.Rows())
                {
                    if (firstHeadRow != 0)
                    {
                        var array = new object[col];
                        for (int y = 1; y <= col; y++)
                        {
                            string value = item.Cell(y).Value.ToString().Trim();
                            string cleanedString = Regex.Replace(value, @"\s+", " ");
                            array[y - 1] = cleanedString;
                        }
                        DTDatos.Rows.Add(array);
                    }
                    firstHeadRow++;
                }

                Result.Tables.Add(DTDatos);
            }

            return Result;
        }

    }
}
